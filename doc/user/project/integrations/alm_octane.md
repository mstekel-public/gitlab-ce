# ALM Octane Service

Go to your project's **Settings > Services > ALM Octane** and fill in the required
details as described in the table below.

| Field | Description |
| ----- | ----------- |
| `alm_octane_url`   | the URL of the target ALM Octane sharedspace, for example: https://node1.octane.com:8080/ui?p=1001. |
| `client_id`    | Client ID used for logging into the ALM Octane server. |
| `client_secret` | Client secret used for logging into the ALM Octane server. |
| `results_pattern` | Search(glob) pattern for test results. |

Once you have configured and enabled ALM Octane integration:

- The target ALM Octane sharedspace starts listing this Gitlab instance as a CI server.
- ALM Octane pipelines can be created for controlling Gitlab pipelines from AML Octane.
- Triggerred by either ALM Octane or Gitlab, the pipeline run reports its status as well as the statuses of its inner jobs
and the results of the tests to ALM Octane for detailed analysis.
