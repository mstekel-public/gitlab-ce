require 'spec_helper'

describe AlmOctaneService, models: true do
  describe 'Associations' do
    it { is_expected.to belong_to :project }
    it { is_expected.to have_one :service_hook }
  end

  describe 'Validations' do
    context 'when service is active' do
      before do
        subject.active = true
      end

      it { is_expected.to validate_presence_of(:alm_octane_url) }
      it { is_expected.to validate_presence_of(:client_id) }
      it { is_expected.to validate_presence_of(:client_secret) }
      it { is_expected.not_to validate_presence_of(:results_pattern) }
      it_behaves_like 'issue tracker service URL attribute', :alm_octane_url
    end

    context 'when service is inactive' do
      before do
        subject.active = false
      end

      it { is_expected.not_to validate_presence_of(:alm_octane_url) }
      it { is_expected.not_to validate_presence_of(:client_id) }
      it { is_expected.not_to validate_presence_of(:client_secret) }
      it { is_expected.not_to validate_presence_of(:results_pattern) }
    end
  end

  describe 'Test' do
    let(:user) { create(:user) }
    let(:project) { create(:project) }
    let(:alm_octane_url) { 'http://octane.com:8081/ui?p=1001' }
    let(:client_id) { 'client_id' }
    let(:client_secret) { 'client_secret' }
    let(:results_pattern) { 'test_results/*.xml' }
    let(:redis_key) { "settings:#{Digest::SHA256.hexdigest(project.web_url).last(40)}" }

    before do
      @alm_octane = described_class.new
      allow(@alm_octane).to receive_messages(
        project: project,
        user: user,
        alm_octane_url: alm_octane_url,
        client_id: client_id,
        client_secret: client_secret,
        results_pattern: results_pattern
      )
      allow(AlmOctaneTaskWorker).to receive(:perform_async)
    end

    it 'initiates long polling queue to ALM Octane' do
      client = double('AlmOctane::Client')
      allow(client).to receive(:connect).and_return(true)
      allow(AlmOctane::Client).to receive(:new).and_return(client)
      expect(AlmOctaneTaskWorker).to receive(:perform_async).with(redis_key, project.id, user.id)
      @alm_octane.test({ project: project, user: user })
    end
  end
end
