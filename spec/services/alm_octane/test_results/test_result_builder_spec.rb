require 'spec_helper'
require 'nokogiri'

describe AlmOctane::TestResults::TestResultBuilder do
  include RepoHelpers

  let(:instance) {described_class}

  describe "#build" do
    context "get test results" do
      it "build JUnut test result xml" do
        xml_paths = [Rails.root.join('spec/services/alm_octane/test_results/j_unit.xml')]
        data = { ci_server_id: 'ci_server_id', job_id: 'ci_name', build_id: 'get_object_id', project_name: 'my_project', result_path: xml_paths }
        expect(instance.build(data)[0]).to eql "<?xml version=\"1.0\"?>\n<test_result>\n  <build server_id=\"ci_server_id\" job_id=\"ci_name\" build_id=\"get_object_id\"/>\n  <test_fields>\n    <test_field type=\"Framework\" value=\"JUnit\"/>\n    <test_field type=\"Test_Level\" value=\"Unit Test\"/>\n  </test_fields>\n  <test_runs>\n    <test_run name=\"test1\" class=\"com.example.actuatorsample.ActuatorSampleApplicationTests\" duration=\"33\" status=\"Failed\">\n      <error type=\"java.lang.AssertionError\" message=\"Emit failure\"/>\n    </test_run>\n    <test_run name=\"test2\" class=\"com.example.actuatorsample.ActuatorSampleApplicationTests\" duration=\"2\" status=\"Passed\"/>\n    <test_run name=\"Api::BasicTasksController and X-API-VERSION, HEADERS set to 1.1.0 #index with 0 basic tasks should return an array under basic_tasks\" class=\"spec.controllers.api.basic_tasks_controller_spec\" duration=\"0\" status=\"Skipped\"/>\n  </test_runs>\n</test_result>\n"
      end

      it "build NUnut test result xml" do
        xml_paths = [Rails.root.join('spec/services/alm_octane/test_results/n_unit.xml')]
        data = { ci_server_id: 'ci_server_id', job_id: 'ci_name', build_id: 'get_object_id', project_name: 'my_project', result_path: xml_paths }
        expect(instance.build(data)[0]).to eql "<?xml version=\"1.0\"?>\n<test_result>\n  <build server_id=\"ci_server_id\" job_id=\"ci_name\" build_id=\"get_object_id\"/>\n  <test_fields>\n    <test_field type=\"Framework\" value=\"NUnit\"/>\n    <test_field type=\"Test_Level\" value=\"Unit Test\"/>\n  </test_fields>\n  <test_runs>\n    <test_run name=\"Pickles.TestHarness.AdditionFeature.AddingSeveralNumbers(System.String[])\" class=\"\" duration=\"0\" status=\"Passed\"/>\n    <test_run name=\"Pickles.TestHarness.AdditionFeature.AddingSeveralNumbers(System.String[])\" class=\"\" duration=\"0\" status=\"Passed\"/>\n    <test_run name=\"Pickles.TestHarness.AdditionFeature.AddTwoNumbers\" class=\"\" duration=\"0\" status=\"Passed\"/>\n    <test_run name=\"Pickles.TestHarness.AdditionFeature.FailToAddTwoNumbers\" class=\"\" duration=\"0\" status=\"Failed\">\n      <error message=\"\"/>\n    </test_run>\n  </test_runs>\n</test_result>\n"
      end
    end
  end
end
