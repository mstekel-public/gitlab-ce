require 'spec_helper'

describe AlmOctane::EventHandler do
  include RepoHelpers

  let(:client) {AlmOctane::Client.new('http://example.com')}

  let(:user) {create(:user, name: 'user')}
  let(:project) {create(:project_empty_repo, :repository, creator: user)}
  let(:repository) {project.repository}
  let(:commit) { project.commit }
  let(:pipeline) do
    create(:ci_pipeline,
           project: project,
           status: 'success',
           sha: project.commit.sha,
           ref: project.default_branch,
           user: user)
  end

  let!(:build) {create(:ci_build, pipeline: pipeline)}

  let(:ci_server_id_key) {"ci-server-id-#{project.id}"}
  let(:alm_octane_url) {'http://octane.com:8081/ui?p=1001'}
  let(:client_id) {'client_id'}
  let(:client_secret) {'client_secret'}
  let(:redis) {double('redis')}
  let(:data_job) do
    Gitlab::DataBuilder::Build.build(build)
  end
  let(:data_pipeline) do
    Gitlab::DataBuilder::Pipeline.build(pipeline)
  end

  let(:instance_ppl) {described_class.new data_pipeline}
  let(:instance_jb) {described_class.new data_job}
  let(:settings) do
    JSON.dump({
                     url: alm_octane_url,
                     client_id: client_id,
                     client_secret: client_secret,
                     server_id: ci_server_id_key,
                     active: true
                 })
  end

  describe "#handle_event" do
    before do
      Sidekiq.redis do |redis|
        allow(redis).to receive(:get).with(anything).and_return(settings)
      end
    end

    context "get event type" do
      it "returns event type from data pipeline" do
        expect(instance_ppl.send :get_event_type).to eq data_pipeline[:object_attributes][:status]
      end

      it "returns event type from data job" do
        expect(instance_jb.send :get_event_type).to eq data_job[:build_status]
      end
    end

    context "get event type" do
      it "returns ci event type from data pipeline" do
        expect(instance_ppl.send :convert_ci_event_type, data_pipeline[:object_attributes][:status]).to eq 'finished'
      end

      it "returns ci event type from data job" do
        expect(instance_jb.send :convert_ci_event_type, data_job[:build_status]).to eq 'queued'
      end
    end

    context "build ci events list" do
      it "returns ci events list for data pipeline" do
        data_pipeline[:object_attributes][:started_at] = Time.now.to_s
        expect((instance_ppl.send :build_ci_events_list, 'finished').events.size).to eq 1
        expect((instance_ppl.send :build_ci_events_list, 'finished').events[0].causes.size).to eq 1
        expect((instance_ppl.send :build_ci_events_list, 'finished').events[0].causes[0].type).to eq 'scm'
      end

      it "returns ci events list for data job" do
        data_job[:build_started_at] = Time.now.to_s
        expect((instance_jb.send :build_ci_events_list, 'queued').events.size).to eq 1
        expect((instance_jb.send :build_ci_events_list, 'queued').events[0].causes.size).to eq 1
        expect((instance_jb.send :build_ci_events_list, 'queued').events[0].causes[0].causes.size).to eq 1
        expect((instance_jb.send :build_ci_events_list, 'queued').events[0].causes[0].causes[0].type).to eq "user"
        expect((instance_jb.send :build_ci_events_list, 'queued').events[0].result).to eq "unavailable"
      end
    end

    context "get scm data" do
      it "returns scm data for data pipeline" do
        data_pipeline[:object_attributes][:started_at] = Time.now.to_s
        data_pipeline[:object_attributes][:before_sha] = commit.parent_ids[1]
        expect((instance_ppl.send :get_scm_data).commits.size).to eq 1
        expect((instance_ppl.send :get_scm_data).commits[0].changes.size).to eq 1
        expect((instance_ppl.send :get_scm_data).commits[0].changes[0].type).to eq 'add'
        expect((instance_ppl.send :get_scm_data).commits[0].user_email).to eq 'job@gitlab.com'
      end
    end
  end
end
