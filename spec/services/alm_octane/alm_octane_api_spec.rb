require 'spec_helper'
require 'json'

describe AlmOctane::AlmOctaneApi do
  include RepoHelpers

  let(:project) {create(:project, :repository)}
  let(:repository) {project.repository}
  let(:current_user) {project.owner}
  let(:master) {'master'}
  let(:feature) {'feature'}
  let(:user1) {create(:user)}
  let(:instance) {described_class.new project, user1}

  describe "#get_job_list" do
    before do
      allow(repository).to receive(:branch_names).and_return([feature, master])
      allow(repository).to receive(:tags).and_return([])
      ref = [instance_double("Branch", name: feature, target: 'abc'),
             instance_double("Branch", name: master, target: 'def')]
      allow(repository).to receive(:branches).and_return(ref)
      allow(repository).to receive(:gitlab_ci_yml_for).with(anything).and_return("111")
    end

    context "get jobs list" do
      it "returns list of jobs from repository" do
        expect(instance.get_job_list).to eql("{\"jobs\":[{\"jobCiId\":\"feature\",\"name\":\"feature\",\"parameters\":[],\"phasesInternal\":[],\"phasesPostBuild\":[]},{\"jobCiId\":\"master\",\"name\":\"master\",\"parameters\":[],\"phasesInternal\":[],\"phasesPostBuild\":[]}]}")
      end
    end
  end

  describe "#get_job_details" do
    before do
      allow(repository).to receive(:branch_names).and_return([feature, master])
      allow(repository).to receive(:ref_names).and_return([feature, master])
      allow(repository).to receive(:tags).and_return([])
      ref = [instance_double("Branch", name: feature, target: 'abc'),
             instance_double("Branch", name: master, target: 'def')]
      allow(repository).to receive(:branches).and_return(ref)
      allow(repository).to receive(:gitlab_ci_yml_for).with(anything).and_return("111")
      allow(project).to receive(:auto_devops_enabled?).and_return(true)
    end

    context "get job detail" do
      it "returns job detail" do
        expect(JSON.parse(instance.get_job_details master).fetch('jobCiId')).to eql("master")
      end
    end
  end
end
