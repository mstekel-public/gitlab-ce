require 'spec_helper'

describe AlmOctane::ConvertUtils do
  let(:instance) {described_class}

  describe ".truncate" do
    context "less than 10" do
      it "returns 'abcdefg" do
        expect(instance.truncate('abcdefg', false, 10)).to eql('abcdefg')
      end
    end

    context "> 10 & left side'" do
      it "returns '... left side" do
        expect(instance.truncate('123456789011', true, 10)).to eql('...1234567')
      end
    end

    context "> 10 & right side'" do
      it "returns '... right side'" do
        expect(instance.truncate('123456789011', false, 10)).to eql('1234567...')
      end
    end
  end

  describe ".convert_value_to_hash" do
    context "convert DTO to hash" do
      it "no array'" do
        obj = AlmOctane::Dto::TestResults::OctaneTestResultTestField.new('Framework', 'NUnit')
        expect((instance.convert_value_to_hash obj).to_json.to_s).to eql("{\"@attributes\":{\"type\":\"Framework\",\"value\":\"NUnit\"}}")
      end
    end

    context "convert DTO to hash" do
      it "with array" do
        obj = AlmOctane::Dto::Scm::ScmData.new('abc', 123, %w(a b))
        expect((instance.convert_value_to_hash obj).to_json.to_s).to eql("{\"repository\":\"abc\",\"builtRevId\":123,\"commits\":[\"a\",\"b\"]}")
      end
    end
  end

  describe ".convert_value_to_hash" do
    context ".generate_xml" do
      it "no attributes'" do
        h = { day: 23, month: 'Dec', year: { 'y' => 1999, 'c' => 21 } }
        expect((instance.generate_xml h).to_s).to eql("<?xml version=\"1.0\"?>\n<root>\n  <day>23</day>\n  <month>Dec</month>\n  <year>\n    <y>1999</y>\n    <c>21</c>\n  </year>\n</root>\n")
      end
    end

    context ".generate_xml" do
      it "convert hash to xml" do
        h = { 'myroot' =>
                 {
                     'nested' => { 'total' => [99, 98], '@attributes' => { 'foo' => 'bar', 'hello' => 'world' } }
                 } }
        expect((instance.generate_xml h).to_s).to eql("<?xml version=\"1.0\"?>\n<myroot>\n  <nested foo=\"bar\" hello=\"world\">\n    <total>99</total>\n    <total>98</total>\n  </nested>\n</myroot>\n")
      end
    end
  end
end
