class AlmOctaneService < Service
  prop_accessor :alm_octane_url, :client_id, :client_secret, :results_pattern

  validates :alm_octane_url, presence: true, url: true, if: :activated?
  validates :client_id, presence: true, if: :activated?
  validates :client_secret, presence: true, if: :activated?

  def title
    'ALM Octane'
  end

  def description
    'Application Lifecycle Management Server'
  end

  def help
    'ALM Octane Server Configuration'
  end

  def self.to_param
    'alm_octane'
  end

  def fields
    [
        { type: 'text', name: 'alm_octane_url', title: 'Server URL', placeholder: 'https://<alm_octane_server>:<port>/ui?p=<sharedspace_id>',
          required: true },
        { type: 'text', name: 'client_id', title: 'Client ID', placeholder: 'Client ID', required: true },
        { type: 'password', name: 'client_secret', placeholder: 'Client Secret', required: true },
        { type: 'text', name: 'results_pattern', placeholder: 'test_results/*.xml', required: false }
    ]
  end

  def self.supported_events
    %w(pipeline)
  end

  def supported_events
    self.class.supported_events + %w(build)
  end

  def execute(data)
    return unless supported_events.include?(data[:object_kind])

    handler = AlmOctane::EventHandler.new(data)
    handler.handle_event
  end

  def test_data(project = nil, user = nil)
    return unless project.present? && user.present?

    { project: project, user: user }
  end

  def test(data)
    project = data[:project]
    return { success: false, result: 'Current project unaccessible' } unless project.present?

    user = data[:user]
    return { success: false, result: 'Current user unaccessible' } unless user.present?

    base_url = URI.join alm_octane_url, '/'
    alm_octane_client = AlmOctane::Client.new(base_url.to_s.chomp!('/'))

    # validate connection
    msg = 'Could not connect to ALM Octane server. Please check your connection parameters.'
    begin
      return { success: false, result: msg } unless alm_octane_client.connect(
        client_id: client_id,
        client_secret: client_secret
      )
    rescue => e
      return { success: false, result: "#{msg} \nError: #{e.message}" }
    end

    server_id = Digest::SHA256.hexdigest(project.web_url).last(40)
    redis_key = "settings:#{server_id}"
    settings = JSON.dump({
                     url: alm_octane_url,
                     client_id: client_id,
                     client_secret: client_secret,
                     results_pattern: results_pattern,
                     server_id: server_id,
                     active: active
                 })

    Sidekiq.redis do |redis|
      redis.set(redis_key, settings)
    end

    AlmOctaneTaskWorker.perform_async(redis_key, project.id, user.id)

    { success: true }
  rescue StandardError => error
    { success: false, result: error }
  end

  def save(*)
    project = Project.find_by(id: project_id)

    if project
      redis_key = "settings:#{Digest::SHA256.hexdigest(project.web_url).last(40)}"
      Sidekiq.redis do |redis|
        settings_string = redis.get(redis_key)
        settings = settings_string.present? ? JSON.parse(settings_string) : {}
        settings['active'] = active
        redis.set(redis_key, JSON.dump(settings))
      end
    end

    super
  end
end
