require 'uri'

class AlmOctaneTaskWorker
  include ApplicationWorker

  queue_namespace :alm_octane
  sidekiq_options retry: false

  def perform(redis_key, project_id, user_id)
    user = User.find_by(id: user_id)
    project = Project.find_by(id: project_id)
    settings = {}
    Sidekiq.redis do |redis|
      settings_string = redis.get(redis_key)

      if settings_string.present?
        settings = JSON.parse(settings_string)

        if settings && settings['active']
          base_url = URI.join(settings['url'], '/')
          client = AlmOctane::Client.new(base_url.to_s.chomp!('/'))

          if client.connect(
            client_id: settings['client_id'],
            client_secret: settings['client_secret']
          )
            shared_space = URI.parse(settings['url']).query.scan(/[^a-zA-Z\d_]*p=(\d+)/)[0][0]
            fetch_next_task_and_respond(client, settings['server_id'], settings['client_id'],
                                        project, shared_space, user)
          else
            logger.error("#{self.class.name} Could not connect to ALM Octane Server")
          end

        end

      end
    end
  ensure
    if settings && settings['active']
      AlmOctaneTaskWorker.perform_async(redis_key, project_id, user_id)
    end
  end

  private

  def fetch_next_task_and_respond(client, server_id, client_id, project, shared_space, user)
    tasks_url = "/internal-api/shared_spaces/#{shared_space}/analytics/ci/servers/#{server_id}/tasks"
    response = fetch_next_task(client, client_id, project, tasks_url)

    if response.code == 200
      task_result = process_task(response[0]['url'], response[0]['id'], project, user)
      respond_with_task_result(client, response, task_result, tasks_url)
    end
  end

  def respond_with_task_result(client, response, task_result, tasks_url)
    client.put("#{tasks_url}/#{response[0]['id']}/result", task_result, true, { content_type: 'application/json' })
  end

  def fetch_next_task(client, client_id, project, tasks_url)
    version = url_encode(Gitlab::VERSION)
    type_name = 'Gitlab'

    client.get(
      "#{tasks_url}?self-type=#{type_name}&self-url=#{url_encode(project.web_url)}"\
      "&plugin-version=#{version}&client_id=#{client_id}",
      true,
      { content_type: 'application/json' }
    )
  end

  def process_task(url, task_id, project, user)
    api = AlmOctane::AlmOctaneApi.new(project, user)
    uri = URI(url)
    run_op = uri.path.split("/").last

    if run_op === 'jobs'
      get_task_result(task_id, api.get_job_list)
    elsif run_op === 'run'
      branch_name = uri.path.split("/").reverse[1]
      get_task_result(task_id, api.run_pipeline(branch_name), 201)
    elsif 'jobs'.eql?(uri.path.split("/").reverse[1])
      get_task_result(task_id, api.get_job_details(run_op))
    else
      message = "#{self.class.name} Unknown operation: #{run_op}"
      logger.info(message)
      get_task_result(task_id, message, 500)
    end

  rescue => e
    msg = "#{self.class.name} Error: #{e.message}"

    if e.backtrace
      msg << " backtrace:  #{e.backtrace.join("\n")}"
    end

    logger.error("#{msg}")
    return get_task_result(0, e.message, 500)
  end

  def get_task_result(task_id, body, status = 200)
    task_result = AlmOctane::Dto::OctaneTaskResult.new(task_id, body, status || 200)
    task_result.convert_to_hash.to_json
  end
end
