require 'builder'
require 'nokogiri'

module AlmOctane
  module ConvertUtils
    def self.convert_to_hash(args)
      hash = {}
      args.each do |key, value|
        hash[key] = convert_value_to_hash(value)
      end

      hash
    end

    def self.convert_value_to_hash(value)
      if value.is_a?(Array)
        value.map {|v| convert_value_to_hash(v)}
      elsif value.respond_to?(:convert_to_hash)
        convert_to_hash value.convert_to_hash
      else
        value
      end
    end

    def self.expose_attr(*names)
      names.each do |name|
        name = name.to_sym
        define_method(name) {attributes[name]}
      end
    end

    # this HASH
    # {'myroot' =>
    #         {
    #             'num' => 99,
    #             'title' => 'something witty',
    #             'nested' => { 'total' => [99, 98], '@attributes' => {'foo' => 'bar', 'hello' => 'world'}},
    #             'anothernest' => {
    #                 '@attributes' => {'foo' => 'bar', 'hello' => 'world'},
    #                 'date' => [
    #                     'today',
    #                     {'day' => 23, 'month' => 'Dec', 'year' => {'y' => 1999, 'c' => 21}, '@attributes' => {'foo' => 'blhjkldsaf'}}
    #                 ]
    #             }
    #     }})
    # will be converted to the following XML:
    # <?xml version="1.0"?>
    # <myroot>
    #   <num>99</num>
    #   <title>something witty</title>
    #   <nested foo="bar" hello="world">
    #     <total>99</total>
    #     <total>98</total>
    #   </nested>
    #   <anothernest foo="bar" hello="world">
    #     <date>today</date>
    #     <date foo="blhjkldsaf">
    #       <day>23</day>
    #       <month>Dec</month>
    #       <year>
    #         <y>1999</y>
    #         <c>21</c>
    #       </year>
    #     </date>
    #   </anothernest>
    # </myroot>

    def self.generate_xml(data, parent = false, opt = {})
      return if data.to_s.empty?
      return unless data.is_a?(Hash)

      unless parent
        # assume that if the hash has a single key that it should be the root
        root, data = data.length == 1 ? data.shift : ["root", data]
        builder = Nokogiri::XML::Builder.new(opt) do |xml|
          xml.send(root) do # rubocop:disable GitlabSecurity/PublicSend
            generate_xml(data, xml)
          end
        end

        return builder.to_xml
      end

      data.each do |label, value|
        if value.is_a?(Hash)
          attrs = value.fetch('@attributes', {})
          # also passing 'text' as a key makes nokogiri do the same thing
          text = value.fetch('@text', '')
          parent.send(label, attrs, text) do # rubocop:disable GitlabSecurity/PublicSend
            value.delete('@attributes')
            value.delete('@text')
            generate_xml(value, parent)
          end

        elsif value.is_a?(Array)
          value.each do |el|
            # lets trick the above into firing so we do not need to rewrite the checks
            el = { label => el }
            generate_xml(el, parent)
          end
        else
          parent.send(label, value) # rubocop:disable GitlabSecurity/PublicSend
        end
      end
    end

    def self.truncate(text, left_side = false, length = 255, truncate_string = '...')
      if text
        l = length - truncate_string.chars.length
        chars = text.chars
        if left_side
          chars.length > length ? (truncate_string + chars[0...l] * '') : text
        else
          chars.length > length ? (chars[0...l] * '' + truncate_string) : text
        end
      end
    end
  end
end
