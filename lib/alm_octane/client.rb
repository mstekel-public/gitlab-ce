module AlmOctane
  class Client
    attr_accessor :server_base_url

    def initialize(server_base_url)
      @server_base_url = server_base_url
    end

    def connect(connection_info)
      return true if connected?

      response = Gitlab::HTTP.post(
        "#{@server_base_url}/authentication/sign_in",
        allow_local_requests: true,
        headers: { 'Content-type' => 'application/json' },
        body: {
          'client_id' => connection_info[:client_id],
          'client_secret' => connection_info[:client_secret]
        }.to_json
      )

      raise StandardError.new(response.body) unless response.success?

      save_cookies(response)
      @connection_info = connection_info
      connected?
    rescue => e
      Rails.logger.error("#{self.class.name}: Error while connecting to #{server_base_url}: #{e.message} backtrace: #{e.backtrace}")
      raise e
    end

    def connected?
      !@lwsso_cookie.nil?
    end

    def post(path, body, resend, options = {})
      return false unless connected?

      response = Gitlab::HTTP.post(
        "#{@server_base_url}#{path}",
        allow_local_requests: true,
        headers: { 'Content-type' => options[:content_type], 'HPECLIENTTYPE' => 'HPE_MQM_UI' },
        body: body
      )
      save_cookies(response)

      return response if response && response.success?

      if response.code == 401 && resend && connect(@connection_info)
        return post(path, body, false, options)
      end
    end

    def put(path, body, resend, options = {})
      return false unless connected?

      response = Gitlab::HTTP.put(
        "#{@server_base_url}#{path}",
        allow_local_requests: true,
        headers: { 'Content-type' => options[:content_type], 'HPECLIENTTYPE' => 'HPE_MQM_UI' },
        body: body
      )
      save_cookies(response)

      return response if response && response.success?

      if response.code == 401 && resend && connect(@connection_info)
        return put(path, body, false, options)
      end
    end

    def get(path, resend, options = {})
      return false unless connected?

      response = Gitlab::HTTP.get(
        "#{@server_base_url}#{path}",
        allow_local_requests: true,
        headers: { 'Content-type' => options[:content_type], 'HPECLIENTTYPE' => 'HPE_MQM_UI' }
      )
      save_cookies(response)

      return response if response && response.success?

      if response.code == 401 && resend && connect(@connection_info)
        return get(path, false, options)
      end

      response
    end

    private

    def save_cookies(response)
      @lwsso_cookie = extract_value_from_cookie(response, 'LWSSO_COOKIE_KEY', @lwsso_cookie)
      @octane_user_cookie = extract_value_from_cookie(response, 'OCTANE_USER', @octane_user_cookie)
      return if response.headers['Set-Cookie'].nil?

      set_cookies = response.headers['Set-Cookie'].split(',').collect { |x| x.strip || x }
      @lwsso_cookie = extract_value_from_set_cookie(set_cookies, 'LWSSO_COOKIE_KEY', @lwsso_cookie)
      @octane_user_cookie = extract_value_from_set_cookie(set_cookies, 'OCTANE_USER', @octane_user_cookie)
      Gitlab::HTTP.default_cookies.add_cookies({
        'LWSSO_COOKIE_KEY' => @lwsso_cookie,
        'OCTANE_USER' => @octane_user_cookie
      })
    end

    def extract_value_from_cookie(response, key, default_value)
      (response && response.headers && response.headers[key]) || default_value
    end

    def extract_value_from_set_cookie(cookies, key, default_value)
      return default_value unless cookies

      cookies.each do |cookie|
        if cookie.start_with? key
          regexp = Regexp.new(key + '=(.*?);')
          match = cookie.match(regexp)
          if match
            return match.captures[0]
          end
        end
      end
      default_value
    end
  end
end
