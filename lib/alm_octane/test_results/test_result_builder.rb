require 'nokogiri'

module AlmOctane
  module TestResults
    class TestResultBuilder
      def self.build(data)
        divide_by_type = {}
        divide_by_type['junit'] = []
        divide_by_type['nunit'] = []
        project_name = data[:project_name]
        data[:result_path].each do |path|
          type = get_test_result_type(path, project_name)
          divide_by_type[type] << path unless type.nil?
        end
        results = []
        divide_by_type.each do |type, paths|
          unless paths.empty?
            data[:result_path] = paths
            results << TestResultFactory.for(type).build(data)
          end
        end

        results
      end

      def self.get_test_result_type(path, project_name)
        xml_file = File.read(path)
        doc = Nokogiri::XML.parse xml_file
        if doc.at 'testsuite'
          'junit'
        elsif doc.at 'test-suite'
          'nunit'
        else
          nil
        end
      end
    end
  end
end
