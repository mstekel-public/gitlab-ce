require 'nokogiri'

module AlmOctane
  module TestResults
    class JunitBuilder
      def build(data)
        return nil if data.empty?

        test_result_build = AlmOctane::Dto::TestResults::OctaneTestResultBuild.new(data[:ci_server_id], data[:job_id], data[:build_id])
        test_fields = get_test_fields
        test_runs = []
        data[:result_path].each do |path|
          test_runs += build_test_result_run(path)
        end
        test_result = AlmOctane::Dto::TestResults::OctaneTestResult.new(test_result_build.convert_to_hash,
                                                                        test_fields,
                                                                        test_runs)
        AlmOctane::ConvertUtils.generate_xml test_result.convert_to_hash
      end

      def build_test_result_run(xml_path)
        xml_file = File.read(xml_path)

        doc = Nokogiri::XML(xml_file, &:noblanks)
        result = []

        if doc.at('testsuites')
          doc.at('testsuites').search('testsuite').each do |testsuite|
            result += build_test_result_set(testsuite)
          end
        else
          result += build_test_result_set(doc.at('testsuite'))
        end

        result
      end

      def get_test_fields
        [AlmOctane::Dto::TestResults::OctaneTestResultTestField.new('Framework', 'JUnit').convert_to_hash,
         # todo:  check if we need it?
         AlmOctane::Dto::TestResults::OctaneTestResultTestField.new('Test_Level', 'Unit Test').convert_to_hash]
      end

      private

      def build_test_result_set(testsuite)
        package = testsuite.attr('package')
        testsuite_result = []
        testsuite.search('testcase').each do |testcase|
          name = testcase.attr('name')
          class_name = testcase.attr('classname')
          duration_time = testcase.attr('time')
          status = get_test_status(testcase)
          start_time = testsuite.attr('timestamp')
          # what time should be taken in case it is not defined in xml? (now it is nil)

          started = if start_time.nil? || start_time.strip.empty?
                      nil
                    else
                      (Time.parse(start_time).to_f * 1000).round
                    end

          duration = if duration_time.nil? || duration_time.strip.empty?
                       0
                     else
                       (duration_time.to_f * 1000).to_i
                     end

          error = if status == 'Failed'
                    build_test_result_error(testcase)
                  else
                    nil
                  end

          # right now we do not support module/component
          testsuite_result << AlmOctane::Dto::TestResults::OctaneTestResultTestRun.new(nil, package, name, class_name,
                                                                                       duration, status, started, error).convert_to_hash
        end
        testsuite_result
      end

      def get_test_status(testcase)
        if testcase.at('failure') || testcase.at('error')
          'Failed'
        elsif testcase.at('skipped') || testcase.attr('status') == 'Inconclusive' # NUnit case
          'Skipped'
        else
          'Passed'
        end
      end

      def build_test_result_error(testcase)
        if testcase.at('failure')
          error_item = 'failure'
        elsif testcase.at('error')
          error_item = 'error'
        else
          return nil
        end

        msg = testcase.at(error_item).attr('message').to_s

        message = if msg.nil? || msg.strip.empty?
                    testcase.at(error_item).content
                  else
                    testcase.at(error_item).attr('message')
                  end

        AlmOctane::Dto::TestResults::OctaneTestResultError.new(testcase.at(error_item).attr('type'), message)
      end
    end
  end
end
