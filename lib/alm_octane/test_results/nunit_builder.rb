require 'nokogiri'

# We are supporting Nunit 3.0 xml report. Nunin xml 2 report might be converted into 3.0

module AlmOctane
  module TestResults
    class NunitBuilder < JunitBuilder
      XSLT = 'lib/alm_octane/test_results/nunit3-junit.xslt'.freeze

      def build_test_result_run(xml_path)
        xslt_file = Rails.root.join(XSLT).to_s
        document = Nokogiri::XML(File.read(xml_path))
        template = Nokogiri::XSLT(File.read(xslt_file))
        transformed_doc = template.transform(document)
        result = []

        if transformed_doc.at('testsuites')
          transformed_doc.at('testsuites').search('testsuite').each do |testsuite|
            result += build_test_result_set(testsuite)
          end
        elsif transformed_doc.at('testsuite')
          result += build_test_result_set(transformed_doc.at('testsuite'))
        else
          result += build_test_result_set(transformed_doc)
        end

        result
      end

      def get_test_fields
        [AlmOctane::Dto::TestResults::OctaneTestResultTestField.new('Framework', 'NUnit').convert_to_hash,
         # todo:  check if we need it?
         AlmOctane::Dto::TestResults::OctaneTestResultTestField.new('Test_Level', 'Unit Test').convert_to_hash]
      end
    end
  end
end
