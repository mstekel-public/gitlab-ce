module AlmOctane
  module TestResults
    class TestResultFactory
      def self.for(type)
        case type
        when 'junit'
          JunitBuilder.new
        when 'nunit'
          NunitBuilder.new
        else
          raise 'Unsupported type of test\'s result'
        end
      end
    end
  end
end
