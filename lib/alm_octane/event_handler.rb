require 'uri'
require 'time'

module AlmOctane
  class EventHandler
    attr_reader :user_name, :event_data, :client_id, :client_secret, :alm_octane_client, :ci_server_id, :alm_octane_url, :results_pattern, :project_name

    INTERNAL_API = "/internal-api/shared_spaces/".freeze
    ANALYTICS_CI_EVENTS = "/analytics/ci/events".freeze
    ANALYTICS_TEST_RESULTS = "/analytics/ci/test-results/".freeze

    def initialize(data)
      @event_data = data
      @user_name = data[:user][:name]
      project_id = data[:object_kind] == 'pipeline' ? data[:project][:id].to_s : data[:project_id].to_s
      project = Project.find_by(id: project_id)
      @project_name = data[:object_kind] == 'pipeline' ? data[:object_attributes][:ref] : data[:ref]
      redis_key = "settings:#{Digest::SHA256.hexdigest(project.web_url).last(40)}"
      Sidekiq.redis do |redis|
        settings_string = redis.get(redis_key)

        unless settings_string
          raise ArgumentError.new('ALM Octane Server settings not found in redis. Try reconfiguring the ALM Octane Service.')
        end

        settings = JSON.parse(settings_string)
        @ci_server_id = settings['server_id']
        @alm_octane_url = settings['url']
        @client_id = settings['client_id']
        @client_secret = settings['client_secret']
        @results_pattern = settings['results_pattern']
        base_url = URI.join @alm_octane_url, '/'
        @alm_octane_client = AlmOctane::Client.new(base_url.to_s.chomp!('/'))
      end
    end

    def handle_event
      event_type = get_event_type
      ci_event_type = convert_ci_event_type(event_type)
      case ci_event_type
      when 'undefined', 'queued'
        return
      else
        event_list = build_ci_events_list(ci_event_type)
        event_list_hash = AlmOctane::ConvertUtils.convert_value_to_hash(event_list)
        payload = event_list_hash.to_json
        send_events(payload)

        if ci_event_type === 'finished' && !pipeline?
          post_test_results
        end
      end
    end

    private

    def post_test_results
      return unless @results_pattern.present?

      build = Ci::Build.find_by_id(get_object_id)
      return if build.nil?

      build.artifacts_file.use_file do |artifacts_path|
        if !artifacts_path.nil? && artifacts_path.ends_with?('.zip')
          temp_path = Dir.mktmpdir('', File.dirname(artifacts_path))

          unless system(*%W(unzip -n #{artifacts_path} -d #{temp_path}))
            raise FailedToExtractError, 'artifacts failed to extract'
          end

          xml_paths = []
          Dir.glob("#{temp_path}/#{@results_pattern}").each do |result_file|
            xml_paths << result_file
          end

          data = { ci_server_id: ci_server_id, job_id: ci_name, build_id: get_object_id, project_name: project_name, result_path: xml_paths }
          test_results_xml = AlmOctane::TestResults::TestResultBuilder.build(data)
          test_results_xml.each do |test_result_xml|
            send_test_results test_result_xml
          end
        end
      end
    end

    def get_project_id
      pipeline? ? event_data[:project][:id].to_s : event_data[:project_id].to_s
    end

    def get_root_name
      pipeline? ? event_data[:object_attributes][:ref] : event_data[:ref]
    end

    def get_root_id
      pipeline? ? event_data[:object_attributes][:id].to_s : event_data[:commit][:id].to_s
    end

    def get_duration
      pipeline? ? event_data[:object_attributes][:duration] : event_data[:build_duration]
    end

    def get_started_at
      pipeline? ? event_data[:object_attributes][:started_at] : event_data[:build_started_at]
    end

    def get_created_at
      pipeline? ? event_data[:object_attributes][:created_at] : event_data[:build_started_at]
    end

    def get_status
      pipeline? ? event_data[:object_attributes][:status] : event_data[:build_status]
    end

    def pipeline?
      event_data[:object_kind] == 'pipeline'
    end

    def ci_name
      pipeline? ? get_root_name : event_data[:build_name]
    end

    def get_user
      event_data[:user][:name]
    end

    def get_user_id
      pipeline? ? event_data[:user][:username] : event_data[:user][:id].to_s
    end

    def get_event_type
      pipeline? ? event_data[:object_attributes][:status] : event_data[:build_status]
    end

    def get_project_display_name
      pipeline? ? event_data[:project][:name] : event_data[:repository][:name]
    end

    def get_object_id
      pipeline? ? event_data[:object_attributes][:id].to_s : event_data[:build_id].to_s
    end

    def send_events(payload)
      shared_space = URI.parse(alm_octane_url).query.scan(/[^a-zA-Z\d_]*p=(\d+)/)[0][0]

      if alm_octane_client.connect(client_id: client_id, client_secret: client_secret)
        event_url = "#{INTERNAL_API}#{shared_space}#{ANALYTICS_CI_EVENTS}"
        alm_octane_client.put(event_url, payload, true, { content_type: 'application/json' })
      end
    end

    def send_test_results(payload)
      shared_space = URI.parse(alm_octane_url).query.scan(/[^a-zA-Z\d_]*p=(\d+)/)[0][0]

      if alm_octane_client.connect(client_id: client_id, client_secret: client_secret)
        event_url = "#{INTERNAL_API}#{shared_space}#{ANALYTICS_TEST_RESULTS}?skip-errors=false"
        alm_octane_client.post(event_url, payload, true, { content_type: 'application/xml' })
      end
    end

    def build_ci_events_list(ci_event_type)
      ci_server_info = get_ci_server_info
      event_list = []
      event_list += get_ci_events(ci_event_type)
      AlmOctane::Dto::Events::CiEventsList.new(ci_server_info, event_list)
    end

    def get_ci_events(ci_event_type)
      event_type = ci_event_type
      build_ci_id = get_object_id
      number = get_object_id
      project = ci_name
      result = convert_ci_build_result(get_status)

      start_time = if get_started_at.nil?
                     get_created_at.nil? ? nil : Time.parse(get_created_at)
                   else
                     Time.parse(get_started_at)
                   end

      estimated_duration = nil
      duration = get_duration
      duration = duration.round(0) unless duration.nil?
      scm_data = nil

      if event_type == 'started' && pipeline?
        scm_data = get_scm_data
        scm_nil = scm_data.nil?

      elsif pipeline?
        scm_nil = event_data[:object_attributes][:before_sha] == Gitlab::Git::BLANK_SHA
      else
        scm_nil = true
      end

      events = []

      causes = get_causes(scm_nil)
      phase_type = pipeline? ? 'post' : 'internal'

      ci_event = if event_type == 'started'
                   AlmOctane::Dto::Events::CiEvent.new(project, event_type, build_ci_id, number,
                                                       project, nil, start_time, estimated_duration,
                                                       nil, nil, phase_type, causes)
                 else
                   AlmOctane::Dto::Events::CiEvent.new(project, event_type, build_ci_id, number,
                                                       project, result, start_time, estimated_duration,
                                                       duration, nil, phase_type, causes)
                 end

      events << ci_event

      unless scm_data.nil?
        scm_event = AlmOctane::Dto::Events::CiEvent.new(nil, 'scm', build_ci_id, nil,
                                                        project, nil, nil, nil,
                                                        nil, scm_data, nil, causes)
        events << scm_event
      end

      events
    end

    def get_causes(scm_nil)
      causes = []

      type = convert_root_ci_causes_type(scm_nil)
      user = type == 'user'
      root_cause = AlmOctane::Dto::Events::CiEventCause.new(type, user ? get_user : nil, user ? get_user_id : nil)
      type = pipeline? ? type : 'upstream'
      if pipeline?
        causes << root_cause
        return causes
      else
        cause = AlmOctane::Dto::Events::CiEventCause.new(type, nil, nil, get_root_name, get_root_id)
        cause.causes << root_cause
        causes << cause
      end

      causes
    end

    def get_ci_server_info
      AlmOctane::Dto::General::CiServerInfo.new('Gitlab', Gitlab::VERSION, alm_octane_client.server_base_url,
                                                ci_server_id, Time.now, Time.now)
    end

    def convert_ci_event_type(status)
      case status
      when 'process', 'enqueue', 'pending', 'created'
        return 'started' if pipeline? && status == 'pending'

        return 'queued'
      when 'success', 'failed', 'canceled', 'skipped'
        'finished'
      when 'running', 'manual'
        return 'undefined' if pipeline? && status == 'running'

        return 'started'
      else
        'undefined'
      end
    end

    def convert_ci_build_result(result)
      case result
      when 'success'
        'success'
      when 'failed'
        'failure'
      when 'drop', 'skipped', 'canceled'
        return 'aborted'
      when 'unstable'
        'unstable'
      else
        'unavailable'
      end
    end

    def convert_root_ci_causes_type(scm_nil)
      if scm_nil
        pipeline_schedule = pipeline? ? event_data[:object_attributes][:pipeline_schedule] : nil

        if !pipeline_schedule.nil? && pipeline_schedule.to_s == 'true'
          'timer'
        else
          'user'
        end

      else
        'scm'
      end
    end

    def get_scm_data
      project_id = event_data[:project][:id]
      project = Project.find_by(id: project_id)
      build_rev_id = event_data[:object_attributes][:sha]
      before_sha = event_data[:object_attributes][:before_sha]
      current_user = User.find_by_username(event_data[:user][:username])
      push_commits = project.repository.commits_between(before_sha, build_rev_id)

      push_commits_count = if push_commits.any?
                             push_commits.size
                           else
                             nil
                           end

      push_data = Gitlab::DataBuilder::Push.build(
        project,
          current_user,
          build_rev_id,
          before_sha,
          event_data[:object_attributes][:ref],
          push_commits,
          push_commits_count)

      scm_repository = AlmOctane::Dto::Scm::ScmRepository.new('git', event_data[:project][:git_http_url], event_data[:object_attributes][:ref])
      scm_commits = []
      push_data[:commits].each do |commit|
        scm_commits << build_commit(commit, build_rev_id)
      end

      unless scm_commits.empty?
        return AlmOctane::Dto::Scm::ScmData.new(scm_repository, build_rev_id, scm_commits)
      end
    end

    def build_commit(commit, build_rev_id)
      time = (Time.parse(commit[:timestamp]).to_f * 1000).round
      user = commit[:author][:name]
      user_email = commit[:author][:email]
      rev_id = commit[:id]
      parent_rev_id = build_rev_id
      comment = commit[:message]
      changes = get_commit_files(commit[:added], commit[:modified], commit[:removed])
      AlmOctane::Dto::Scm::ScmCommit.new(time, user, rev_id, changes, user_email, parent_rev_id, comment)
    end

    def get_commit_files(added, modified, removed)
      added_files = []
      if added.any?
        added.each do |file|
          added_files << AlmOctane::Dto::Scm::ScmCommitFileChange.new('add', file)
        end
      end

      modified_files = []
      if modified.any?
        modified.each do |file|
          modified_files << AlmOctane::Dto::Scm::ScmCommitFileChange.new('edit', file)
        end
      end

      removed_files = []
      if removed.any?
        removed.each do |file|
          removed_files << AlmOctane::Dto::Scm::ScmCommitFileChange.new('delete', file)
        end
      end

      [added_files, modified_files, removed_files].compact.reduce([], :|)
    end
  end
end
