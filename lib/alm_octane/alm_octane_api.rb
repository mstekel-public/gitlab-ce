require 'json'

module AlmOctane
  class AlmOctaneApi
    include API::Helpers
    attr_reader :project, :current_user, :config_processor, :jobs, :stages

    def initialize(project, current_user, trigger = nil)
      @current_user = current_user
      @trigger = trigger
      @project = project
    end

    def get_job_list
      # no configured pipelines return empty list.
      ci_jobs = get_pipeline_node_list
      array_hash = { "jobs" => AlmOctane::ConvertUtils.convert_value_to_hash(ci_jobs.get_list) }
      array_hash.to_json
    end

    def get_job_details(job_id)
      # no configured pipelines return empty list.
      ci_jobs = get_pipeline_node_list
      pipeline_node = ci_jobs.find job_id
      return {}.to_json if pipeline_node.nil?

      set_config_processor pipeline_node.name
      @jobs = @config_processor.jobs
      @stages = @config_processor.stages

      topology = pipeline_topology
      topology.each_pair do |stage, job_list|
        build_stage_topology(pipeline_node, stage, job_list)
      end

      hash = AlmOctane::ConvertUtils.convert_value_to_hash pipeline_node
      hash.to_json
    end

    def run_pipeline(branch)
      ci_jobs = get_pipeline_node_list
      branch_name = get_branch_name branch

      raise ArgumentError.new("The branch refname #{branch_name} is invalid") unless ci_jobs.find branch

      Ci::CreatePipelineService.new(project, current_user, ref: branch_name).execute(:external)
      'Job Started'
    end

    private

    def get_pipeline_by_id(id)
      project.pipelines.find_by!(id: id).present(current_user: current_user)
    end

    def retry_by_pipeline_id(id)
      pipeline = get_pipeline_by_id(id)
      pipeline.retry_failed(current_user) unless pipeline.nil?
    end

    def cancel_by_pipeline_id(id)
      pipeline = get_pipeline_by_id id
      pipeline.cancel_running unless pipeline.nil?
    end

    def get_ci_job_id(job_name)
      job_name
    end

    def get_project_id(ci_job_id)
      ci_job_id.split("-")[0].to_i
    end

    def get_branch_name(ci_job_id)
      ci_job_id
    end

    def get_pipeline_node_list
      job_names = project.repository.ref_names
      return build_pipeline_node_list job_names if project.auto_devops_enabled?

      job_names.clear
      Gitlab::GitalyClient.allow_n_plus_1_calls do
        project.repository.branches.each do |branch|
          job_names << branch.name unless project.repository.gitlab_ci_yml_for(branch.target).nil?
        end
        project.repository.tags.each do |tag|
          target_commit = Gitlab::Git::Commit.find(project.repository, tag.target)
          job_names << tag.name unless project.repository.gitlab_ci_yml_for(target_commit.sha).nil?
        end
        build_pipeline_node_list(job_names)
      end
    end

    def build_pipeline_node_list(job_names)
      ci_jobs = AlmOctane::Dto::CiJobList.new
      job_names.each {|job_name| ci_jobs.add(AlmOctane::Dto::PipelineNode.new(get_ci_job_id(job_name), job_name))}
      ci_jobs.get_list.sort_by(&:name)
      ci_jobs
    end

    def set_config_processor(branch)
      return unless ci_yaml_file branch
      return @config_processor if defined?(@config_processor)

      @config_processor ||= Gitlab::Ci::YamlProcessor.new(ci_yaml_file branch)
    end

    def ci_yaml_file(branch)
      ci_yaml_file =
        if project.auto_devops_enabled?
          implied_ci_yaml_file
        else
          ci_yaml_from_repo branch
        end

      if ci_yaml_file
        ci_yaml_file
      else
        nil
      end
    end

    def implied_ci_yaml_file
      return unless project

      if project.auto_devops_enabled?
        Gitlab::Template::GitlabCiYmlTemplate.find('Auto-DevOps').content
      end
    end

    def ci_yaml_from_repo(ref)
      return unless project

      branch = project.repository.find_branch(ref)

      if branch.nil?
        tag = project.repository.find_tag ref
        unless tag.nil?
          target_commit = Gitlab::Git::Commit.find(project.repository, tag.target)
          sha = target_commit.sha
        end
      else
        sha = branch.target
      end

      return nil if sha.nil?

      project.repository.gitlab_ci_yml_for(sha, ci_yaml_file_path)
    rescue GRPC::NotFound, GRPC::Internal
      nil
    end

    def ci_yaml_file_path
      project.ci_config_path.presence || '.gitlab-ci.yml'
    end

    def pipeline_stage_builds(stage)
      selected_jobs = jobs.select {|_, job| job[:stage] == stage}

      job_list = []
      selected_jobs.each do |_, job|
        job_list << job[:name].to_s
      end
      job_list
    end

    def pipeline_topology
      topology = {}
      stages.each do |stage|
        topology[stage] = pipeline_stage_builds stage
      end
      topology
    end

    def build_stage_topology(root, stage, job_list)
      stage_phase = AlmOctane::Dto::PipelinePhase.new(stage, true)
      root.phase_internal << stage_phase
      job_list.each do |job|
        stage_phase.jobs << AlmOctane::Dto::PipelineNode.new(job.to_s, job.to_s)
      end
    end
  end
end
