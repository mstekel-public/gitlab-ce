module AlmOctane
  module Dto
    module General
      class CiServerInfo
        extend ConvertUtils
        attr_reader :type, :version, :url, :instance_id, :instance_id_from, :sending_time

        def initialize(type, version, url, instance_id, instance_id_from, sending_time)
          @type = type
          @version = version
          @url = url
          @instance_id = instance_id
          @instance_id_from = instance_id_from
          @sending_time = sending_time
        end

        def convert_to_hash
          hash = {
              type: type,
              version: version,
              url: url,
              instanceId: instance_id,
              instanceIdFrom: (instance_id_from.to_f * 1000).to_i,
              sendingTime: (sending_time.to_f * 1000).to_i
          }
          hash
        end
      end
    end
  end
end
