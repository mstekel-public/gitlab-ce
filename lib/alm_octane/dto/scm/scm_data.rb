module AlmOctane
  module Dto
    module Scm
      class ScmData
        extend ConvertUtils
        attr_reader :repository, :built_rev_id, :commits

        def initialize(repository, built_rev_id, commits = [])
          @repository = repository
          @built_rev_id = built_rev_id
          @commits = commits
        end

        def convert_to_hash
          hash = {
              repository: repository,
              builtRevId: built_rev_id,
              commits: commits
          }
          hash
        end
      end
    end
  end
end
