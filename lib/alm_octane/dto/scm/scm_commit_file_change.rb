module AlmOctane
  module Dto
    module Scm
      class ScmCommitFileChange
        extend ConvertUtils
        attr_reader :type, :file

        def initialize(type, file)
          @type = type
          @file = file
        end

        def convert_to_hash
          hash = {
              type: type,
              file: file
          }
          hash
        end
      end
    end
  end
end
