module AlmOctane
  module Dto
    module Scm
      class ScmRepository
        extend ConvertUtils
        attr_reader :type, :url, :branch

        def initialize(type, url, branch)
          @type = type
          @url = url
          @branch = branch
        end

        def convert_to_hash
          hash = {
              type: type,
              url: url,
              branch: branch
          }
          hash
        end
      end
    end
  end
end
