module AlmOctane
  module Dto
    module Scm
      class ScmCommit
        extend ConvertUtils
        attr_reader :time, :user, :commits, :user_email, :rev_id, :parent_rev_id,
                    :comment, :changes

        def initialize(time, user, rev_id, changes = [], user_email = nil, parent_rev_id = nil, comment = nil)
          @time = time
          @user = user
          @user_email = user_email
          @rev_id = rev_id
          @parent_rev_id = parent_rev_id
          @comment = comment
          @changes = changes
        end

        def convert_to_hash
          hash = {
              time: time,
              user: user,
              userEmail: user_email,
              revId: rev_id,
              parentRevId: parent_rev_id,
              comment: comment,
              changes: changes
          }
          hash.reject { |k, v| v.nil? }
        end
      end
    end
  end
end
