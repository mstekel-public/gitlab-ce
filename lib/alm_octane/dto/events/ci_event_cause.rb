module AlmOctane
  module Dto
    module Events
      class CiEventCause
        extend ConvertUtils
        attr_accessor :type, :user_name, :user_id, :causes, :project, :build_ci_id

        def initialize(type, user_name = nil, user_id = nil, project = nil, build_ci_id = nil, causes = [])
          @type = type
          @user_name = user_name
          @user_id = user_id
          @project = project
          @build_ci_id = build_ci_id
          @causes = causes
        end

        def convert_to_hash
          hash = {
              type: type,
              userName: user_name,
              userId: user_id,
              project: project,
              buildCiId: build_ci_id,
              causes: causes
          }
          hash.reject { |k, v| v.nil? }
        end
      end
    end
  end
end
