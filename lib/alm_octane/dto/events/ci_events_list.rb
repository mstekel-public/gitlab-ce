module AlmOctane
  module Dto
    module Events
      class CiEventsList
        extend ConvertUtils
        attr_reader :server, :events

        def initialize(server, events = [])
          @server = server
          @events = events
        end

        def convert_to_hash
          hash = {
              server: server,
              events: events
          }
          hash
        end
      end
    end
  end
end
