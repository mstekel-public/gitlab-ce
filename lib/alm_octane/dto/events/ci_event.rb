module AlmOctane
  module Dto
    module Events
      class CiEvent
        # rubocop:disable Metrics/ParameterLists
        extend ConvertUtils
        attr_reader :project_display_name, :ci_event_type, :build_ci_id, :number, :project,
                    :result, :start_time, :estimated_duration, :duration,
                    :scm_data, :phase_type, :causes, :parameters

        def initialize(
            project_display_name, ci_event_type, build_ci_id, number, project,
            result, start_time, estimated_duration, duration,
            scm_data, phase_type, causes = nil,
            parameters = nil)

          @project_display_name = project_display_name
          @ci_event_type = ci_event_type
          @build_ci_id = build_ci_id
          @number = number
          @project = project
          @causes = causes
          @parameters = parameters
          @result = result
          @start_time = start_time
          @estimated_duration = estimated_duration
          @duration = duration
          @scm_data = scm_data
          @phase_type = phase_type
        end

        def convert_to_hash
          hash = {
              projectDisplayName: project_display_name,
              eventType: ci_event_type,
              buildCiId: build_ci_id.to_s,
              number: number,
              project: project,
              causes: causes,
              parameters: parameters,
              result: result,
              startTime: start_time.nil? ? nil : (start_time.to_f * 1000).to_i,
              estimatedDuration: estimated_duration,
              duration: duration.nil? ? nil : duration * 1000,
              scmData: scm_data,
              phaseType: phase_type
          }
          hash.reject { |k, v| v.nil? }
        end
      end
    end
  end
  # rubocop:enable Metrics/ParameterLists
end
