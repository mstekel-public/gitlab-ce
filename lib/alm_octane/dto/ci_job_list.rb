module AlmOctane
  module Dto
    class CiJobList
      attr_reader :list

      def initialize(*args)
        @list = []
      end

      def add(item)
        list << item
      end

      def find(job_ci_id)
        list.detect {|pipeline_node| pipeline_node.job_ci_id.casecmp(job_ci_id) == 0}
      end

      def get_list
        list
      end
    end
  end
end
