module AlmOctane
  module Dto
    module TestResults
      class OctaneTestResultTestRun
        # rubocop:disable Metrics/ParameterLists
        extend ConvertUtils
        attr_reader :module_name, :package_name, :name, :class_name, :duration, :status, :started, :error, :external_report_url

        def initialize(module_name, package_name, name, class_name, duration, status, started, error, external_report_url = nil)
          @module_name = AlmOctane::ConvertUtils.truncate module_name
          @package_name = AlmOctane::ConvertUtils.truncate package_name
          @name = AlmOctane::ConvertUtils.truncate(name, true)
          @class_name = AlmOctane::ConvertUtils.truncate(class_name, true)
          @duration = duration
          @status = status
          @started = started
          @error = error
          @external_report_url = external_report_url
        end

        def convert_to_hash
          attr_hash = {
              'module': module_name,
              'package': package_name,
              'name': name,
              'class': class_name,
              'duration': duration,
              'status': status,
              'started': started,
              'external_report_url': external_report_url
          }
          attr_hash = attr_hash.reject { |k, v| v.nil? }
          if error.nil?
            { '@attributes' => attr_hash }
          else
            [{ 'error' => error.convert_to_hash, '@attributes' => attr_hash }]
          end
        end
      end
    end
  end
  # rubocop:enable Metrics/ParameterLists
end
