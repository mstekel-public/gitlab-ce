module AlmOctane
  module Dto
    module TestResults
      class OctaneTestResultBuild
        extend ConvertUtils
        attr_reader :server_id, :job_id, :build_id

        def initialize(server_id, job_id, build_id)
          @server_id = server_id
          @job_id = job_id
          @build_id = build_id
        end

        def convert_to_hash
          {
             '@attributes' => { 'server_id': server_id, 'job_id': job_id,
                                'build_id': build_id }
          }
        end
      end
    end
  end
end
