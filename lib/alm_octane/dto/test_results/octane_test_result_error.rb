module AlmOctane
  module Dto
    module TestResults
      class OctaneTestResultError
        extend ConvertUtils
        attr_reader :type, :message, :stack_trace

        def initialize(type, message, stack_trace = nil)
          @type = type
          @message = message
          @stack_trace = stack_trace
        end

        def convert_to_hash
          hash = {
              'type': type,
              'message': message
            # todo: check if we need it and what to do...
            # 'StackTrace': stack_trace
          }

          error_hash = { '@attributes' => hash.reject { |k, v| v.nil? } }
          error_hash.empty? ? nil : error_hash
        end
      end
    end
  end
end
