module AlmOctane
  module Dto
    module TestResults
      class OctaneTestResultTestField
        extend ConvertUtils
        attr_reader :type, :value

        def initialize(type, value)
          @type = type
          @value = value
        end

        def convert_to_hash
          { '@attributes' => { 'type': type,
                               'value': value } }
        end
      end
    end
  end
end
