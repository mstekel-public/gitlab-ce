module AlmOctane
  module Dto
    module TestResults
      class OctaneTestResult
        extend ConvertUtils
        attr_reader :build, :test_fields, :test_runs

        def initialize(build, test_fields, test_runs)
          @build = build
          @test_fields = test_fields
          @test_runs = test_runs
        end

        def convert_to_hash
          {
              'test_result' => {
                  'build' => build,
                  'test_fields' => { 'test_field' => [test_fields] },
                  'test_runs' => { 'test_run' => [test_runs] }
              }
          }
        end
      end
    end
  end
end
