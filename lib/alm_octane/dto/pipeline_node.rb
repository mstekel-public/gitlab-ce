module AlmOctane
  module Dto
    class PipelineNode
      extend ConvertUtils

      attr_reader :job_ci_id, :name
      attr_accessor :parameters, :phase_internal, :phase_post_build

      def initialize(job_ci_id, name, phase_internal = [], phase_post_build = [], parameters = [])
        @job_ci_id = job_ci_id
        @name = name
        @parameters = parameters
        @phase_internal = phase_internal
        @phase_post_build = phase_post_build
      end

      def convert_to_hash
        hash = {
            jobCiId: job_ci_id,
            name: name,
            parameters: parameters,
            phasesInternal: phase_internal,
            phasesPostBuild: phase_post_build
        }
        hash
      end
    end
  end
end
