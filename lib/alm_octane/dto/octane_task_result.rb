module AlmOctane
  module Dto
    class OctaneTaskResult
      extend ConvertUtils
      attr_reader :task_id, :status, :body

      def initialize(task_id, body, status)
        @task_id = task_id
        @status = status
        @body = body || {}.to_json
      end

      def convert_to_hash
        hash = {
            status: status.to_s,
            id: task_id,
            body: body
        }
        hash
      end
    end
  end
end
