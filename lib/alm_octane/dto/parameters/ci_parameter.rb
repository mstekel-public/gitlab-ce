module AlmOctane
  module Dto
    module Parameters
      class CiParameter
        extend ConvertUtils
        attr_reader :type, :user, :default_value

        def initialize(type, name, description, default_value)
          @type = type
          @name = name
          @description = description
          @default_value = default_value
        end

        def convert_to_hash
          hash = {
              type: type,
              name: name,
              description: description,
              defaultValue: default_value
          }
          hash
        end
      end
    end
  end
end
