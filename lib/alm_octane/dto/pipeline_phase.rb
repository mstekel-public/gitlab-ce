module AlmOctane
  module Dto
    class PipelinePhase
      extend ConvertUtils

      attr_reader :name, :blocking
      attr_accessor :jobs

      def initialize(name, blocking, jobs = [])
        @blocking = blocking
        @name = name
        @jobs = jobs
      end

      def convert_to_hash
        hash = {
            blocking: blocking,
            name: name,
            jobs: jobs
        }
        hash
      end
    end
  end
end
